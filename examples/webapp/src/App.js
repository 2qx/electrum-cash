import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

import React from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { ElectrumCluster, ElectrumTransport } from 'electrum-cash';

class App extends React.Component {
  state = {
    // Initialize an electrum cluster where 2 out of 3 needs to be consistent, polled randomly with fail-over (default).
    electrum: new ElectrumCluster('Electrum cluster example', '1.4.1', 2, 3),

    // Example TXID
    txid: '4db095f34d632a4daf942142c291f1f2abb5ba2e1ccac919d85bdc2f671fb251',

    // Empty TX hash, will be filled by request
    txhash: '',
  };

  async componentDidMount() {
    // Add some servers to the cluster.
    this.state.electrum.addServer('bch.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
    this.state.electrum.addServer('electroncash.de', 60002, ElectrumTransport.WSS.Scheme);
    this.state.electrum.addServer('blackie.c3-soft.com', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
    this.state.electrum.addServer('bch.loping.net', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
    this.state.electrum.addServer('electrum.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
  }

  async getTxHash(txid) {
    // Wait for the electrum cluster to be ready
    await this.state.electrum.ready();

    // Request the transaction hash for the passed TXID
    const txhash = await this.state.electrum.request('blockchain.transaction.get', txid);

    // Update the state with the returned TX hash
    this.setState({ txhash: txhash.toString() });
  }

  updateTxId(txid) {
    this.setState({ txid });
  }

  render() {
    return (
      <Container fluid className="App">
        <Row className="Header">
          <Col></Col>
          <Col>Get Transaction Hex String</Col>
          <Col></Col>
        </Row>
        <Row>
          <Col></Col>
          <Col>
            <Form.Group className="form">
              <Form.Label>Transaction ID</Form.Label>
              <Form.Control value={this.state.txid || ''} onChange={(event) => this.updateTxId(event.target.value)}></Form.Control>
              <Button onClick={() => this.getTxHash(this.state.txid)}>Get details</Button>
            </Form.Group>
          </Col>
          <Col></Col>
        </Row>
        <Row>
          <Col></Col>
          <Col>{ this.state.txhash }</Col>
          <Col></Col>
        </Row>
      </Container>
    );
  }
}

export default App;
