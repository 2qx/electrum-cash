import { ClusterDistribution, ClusterOrder } from './enums';
import { TransportScheme } from './interfaces';

/**
 * Object containing the commonly used ports and schemes for specific Transports.
 * @example const electrum = new ElectrumClient('Electrum client example', '1.4.1', 'bch.imaginary.cash', Transport.WSS.Port, Transport.WSS.Scheme);
 *
 * @property {object} TCP       Port and Scheme to use unencrypted TCP sockets.
 * @property {object} TCP_TLS   Port and Scheme to use TLS-encrypted TCP sockets.
 * @property {object} WS        Port and Scheme to use unencrypted WebSockets.
 * @property {object} WSS       Port and Scheme to use TLS-encrypted WebSockets.
 */
export const ElectrumTransport =
{
	TCP:     { Port: 50001, Scheme: 'tcp' as TransportScheme },
	TCP_TLS: { Port: 50002, Scheme: 'tcp_tls' as TransportScheme },
	WS:      { Port: 50003, Scheme: 'ws' as TransportScheme },
	WSS:     { Port: 50004, Scheme: 'wss' as TransportScheme },
};

export const DefaultParameters =
{
	// Port number for TCP TLS connections
	PORT: ElectrumTransport.TCP_TLS.Port,

	// Transport to connect to the Electrum server
	TRANSPORT_SCHEME: ElectrumTransport.TCP_TLS.Scheme,

	// How long to wait before attempting to reconnect, in milliseconds.
	RECONNECT: 15 * 1000,

	// How long to wait for network operations before following up, in milliseconds.
	TIMEOUT: 120 * 1000,

	// Time between ping messages, in milliseconds. Pinging keeps the connection alive.
	// The reason for pinging this frequently is to detect connection problems early.
	PING_INTERVAL: 3 * 1000,

	// How many servers are required before we trust information provided.
	CLUSTER_CONFIDENCE: 1,

	// How many servers we send requests to.
	CLUSTER_DISTRIBUTION: ClusterDistribution.ALL,

	// What order we select servers to send requests to.
	CLUSTER_ORDER: ClusterOrder.RANDOM,
};
