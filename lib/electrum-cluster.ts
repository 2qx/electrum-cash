import debug from './util';
import ElectrumClient from './electrum-client';
import { ClusterOrder, ClusterDistribution, ClusterStatus, ClientState, ConnectionStatus } from './enums';
import { DefaultParameters } from './constants';
import { Mutex } from 'async-mutex';
import { EventEmitter } from 'events';
import type { RPCParameter } from './rpc-interfaces';
import type { ClusterStrategy, ClientConfig, ResolveFunction, RejectFunction, SubscribeCallback, RequestResponse, TransportScheme } from './interfaces';

/**
 * Triggers when the cluster connects to enough servers to satisfy both the cluster confidence and distribution policies.
 *
 * @event ElectrumCluster#ready
 */

/**
 * Triggers when the cluster loses a connection and can no longer satisfy the cluster distribution policy.
 *
 * @event ElectrumCluster#degraded
 */

/**
 * Triggers when the cluster loses a connection and can no longer satisfy the cluster confidence policy.
 *
 * @event ElectrumCluster#disabled
 */

/**
 * High-level electrum client that provides transparent load balancing, confidence checking and/or low-latency polling.
 */
class ElectrumCluster extends EventEmitter
{
	// Declare instance variables
	strategy: ClusterStrategy;

	// Initialize an empty dictionary of clients in the cluster
	clients: { [index: string]: ClientConfig } = {};

	// Start at 0 connected clients
	connections = 0;

	// Set up an empty set of notification data.
	notifications: { [index: string]: number } = {};

	// Start the cluster in DISABLED state
	status = ClusterStatus.DISABLED;

	// Start counting request IDs at 0
	requestCounter = 0;

	// Initialize an empty dictionary for keeping track of request resolvers
	requestPromises: { [index: number]: Promise<Error | RequestResponse>[] } = {};

	// Lock to prevent concurrency race conditions when sending requests.
	requestLock = new Mutex();

	// Lock to prevent concurrency race conditions when receiving responses.
	responseLock = new Mutex();

	/**
	 * @param {string} application    your application name, used to identify to the electrum hosts.
	 * @param {string} version        protocol version to use with the hosts.
	 * @param {number} confidence     wait for this number of hosts to provide identical results.
	 * @param {number} distribution   request information from this number of hosts.
	 * @param {ClusterOrder} order    select hosts to communicate with in this order.
	 * @param {number} timeout        how long network delays we will wait for before taking action, in milliseconds.
	 * @param {number} pingInterval      the time between sending pings to the electrum host, in milliseconds.
	 */
	constructor(
		public application: string,
		public version: string,
		confidence: number = DefaultParameters.CLUSTER_CONFIDENCE,
		distribution: number = DefaultParameters.CLUSTER_DISTRIBUTION,
		order: ClusterOrder = DefaultParameters.CLUSTER_ORDER,
		public timeout: number = DefaultParameters.TIMEOUT,
		public pingInterval: number = DefaultParameters.PING_INTERVAL,
	)
	{
		// Initialize the event emitter.
		super();

		// Initialize strategy.
		this.strategy =
		{
			distribution: distribution,
			confidence: confidence,
			order: order,
		};

		// Write a log message.
		debug.cluster(`Initialized empty cluster (${confidence} of ${distribution || 'ALL'})`);

		// Print out a warning if we cannot guarantee consensus for subscription notifications.
		// Case 1: we don't know how many servers will be used, so warning just to be safe
		// Case 2: we know the number of servers needed to trust a response is less than 50%.
		if((distribution === ClusterDistribution.ALL) || (confidence / distribution <= 0.50))
		{
			debug.warning(`Subscriptions might return multiple valid responses when confidence (${confidence}) is less than 51% of distribution.`);
		}
	}

	/**
	 * Adds a server to the cluster.
	 *
	 * @param {string} host              fully qualified domain name or IP number of the host.
	 * @param {number} port              the TCP network port of the host.
	 * @param {TransportScheme} scheme   the transport scheme to use for connection
	 * @param {boolean} autoConnect      flag indicating whether the server should automatically connect (default true)
	 *
	 * @throws {Error} if the cluster's version is not a valid version string.
	 * @returns a promise that resolves when the connection has been initiated.
	 */
	async addServer(
		host: string,
		port: number = DefaultParameters.PORT,
		scheme: TransportScheme = DefaultParameters.TRANSPORT_SCHEME,
		autoConnect: boolean = true,
	): Promise<void>
	{
		// Set up a new electrum client.
		const client = new ElectrumClient(
			this.application,
			this.version,
			host,
			port,
			scheme,
			this.timeout,
			this.pingInterval,
		);

		// Store this client.
		this.clients[`${host}:${port}`] =
		{
			state: ClientState.UNAVAILABLE,
			connection: client,
		};

		/**
		 * Define a helper function to evaluate and log cluster status.
		 *
		 * @fires ElectrumCluster#ready
		 * @fires ElectrumCluster#degraded
		 * @fires ElectrumCluster#disabled
		 */
		const updateClusterStatus = (): void =>
		{
			// Calculate the required distribution, taking into account that distribution to all is represented with 0.
			const distribution = Math.max(this.strategy.confidence, this.strategy.distribution);

			// Check if we have enough connections to saturate distribution.
			if(this.connections >= distribution)
			{
				// If the cluster is not currently considered ready..
				if(this.status !== ClusterStatus.READY)
				{
					// Mark the cluster as ready.
					this.status = ClusterStatus.READY;

					// Emit the ready signal to indicate the cluster is running in a ready mode.
					this.emit('ready');

					// Write a log message with an update on the current cluster status.
					debug.cluster(`Cluster status is ready (currently ${this.connections} of ${distribution} connections available.)`);
				}
			}

			// If we still have enough available connections to reach confidence..
			else if(this.connections >= this.strategy.confidence)
			{
				// If the cluster is not currently considered degraded..
				if(this.status !== ClusterStatus.DEGRADED)
				{
					// Mark the cluster as degraded.
					this.status = ClusterStatus.DEGRADED;

					// Emit the degraded signal to indicate the cluster is running in a degraded mode.
					this.emit('degraded');

					// Write a log message with an update on the current cluster status.
					debug.cluster(`Cluster status is degraded (only ${this.connections} of ${distribution} connections available.)`);
				}
			}

			// If we don't have enough connections to reach confidence..
			// .. and the cluster is not currently considered disabled..
			else if(this.status !== ClusterStatus.DISABLED)
			{
				// Mark the cluster as disabled.
				this.status = ClusterStatus.DISABLED;

				// Emit the degraded signal to indicate the cluster is disabled.
				this.emit('disabled');

				// Write a log message with an update on the current cluster status.
				debug.cluster(`Cluster status is disabled (only ${this.connections} of the ${distribution} connections are available.)`);
			}
		};

		// Define a function to run when client has connects.
		const onConnect = async (): Promise<void> =>
		{
			// Wrap in a try-catch so we can ignore errors.
			try
			{
				// Check connection status
				const connectionStatus = client.connection.status;

				// If the connection is fine..
				if(connectionStatus === ConnectionStatus.CONNECTED)
				{
					// If this was from an unavailable connection..
					if(this.clients[`${host}:${port}`].state === ClientState.UNAVAILABLE)
					{
						// Update connection counter.
						this.connections += 1;
					}

					// Set client state to available.
					this.clients[`${host}:${port}`].state = ClientState.AVAILABLE;

					// update the cluster status.
					updateClusterStatus();
				}
			}
			catch(error)
			{
				// Do nothing.
			}
		};

		// Define a function to run when client disconnects.
		const onDisconnect = (): void =>
		{
			// If this was from an established connection..
			if(this.clients[`${host}:${port}`].state === ClientState.AVAILABLE)
			{
				// Update connection counter.
				this.connections -= 1;
			}

			// Set client state to unavailable.
			this.clients[`${host}:${port}`].state = ClientState.UNAVAILABLE;

			// update the cluster status.
			updateClusterStatus();
		};

		// Set up handlers for connection and disconnection.
		client.connection.on('connect', onConnect.bind(this));
		client.connection.on('disconnect', onDisconnect.bind(this));

		// Connect if auto-connect is set to true, returning the connection result.
		if(autoConnect)
		{
			// Set up the connection.
			await client.connect();
		}
	}

	/**
	 * Calls a method on the remote server with the supplied parameters.
	 *
	 * @param {string}    method       name of the method to call.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if not enough clients are connected
	 * @throws {Error} if no response is received with sufficient integrity
	 * @returns a promise that resolves with the result of the method.
	 */
	async request(method: string, ...parameters: RPCParameter[]): Promise<Error | RequestResponse>
	{
		// Check if the cluster is unable to serve requests.
		if(this.status === ClusterStatus.DISABLED)
		{
			throw(new Error(`Cannot request '${method}' when available clients (${this.connections}) is less than required confidence (${this.strategy.confidence}).`));
		}

		// Lock this request method temporarily.
		const unlock = await this.requestLock.acquire();

		// Declare requestId outside of try-catch scope.
		let requestId = 0;

		// NOTE: If this async method is called very rapidly, it's theoretically possible that the parts below could interfere.
		try
		{
			// Increase the current request counter.
			this.requestCounter += 1;

			// Copy the request counter so we can work with the copy and know it won't change
			// even if the request counter is raised from concurrent requests.
			requestId = this.requestCounter;
		}
		finally
		{
			// Unlock this request method now that the concurrency sensitive condition is completed.
			unlock();
		}

		// Initialize an empty list of request promises.
		this.requestPromises[requestId] = [];

		// Extract all available client IDs
		const availableClientIDs = Object.keys(this.clients)
			.filter((clientID) => this.clients[clientID].state === ClientState.AVAILABLE);

		// Initialize a sent counter.
		let sentCounter = 0;

		// Determine the number of clients we need to send to, taking ClusterDistribution.ALL (=0) into account.
		let requiredDistribution = (this.strategy.distribution || availableClientIDs.length);

		// If the cluster is in degraded status, we do not have enough available clients to
		// match distribution, but still enough to reach consensus, so we use the clients we have.
		if(this.status === ClusterStatus.DEGRADED)
		{
			requiredDistribution = availableClientIDs.length;
		}

		// Repeat until we have sent the request to the desired number of clients.
		while(sentCounter < requiredDistribution)
		{
			// Pick an array index according to our ordering strategy.
			let currentIndex = 0;

			// Use a random array index when cluster order is set to RANDOM
			if(this.strategy.order === ClusterOrder.RANDOM)
			{
				currentIndex = Math.floor(Math.random() * availableClientIDs.length);
			}

			// Move a client identity from the client list to its own variable.
			const [ currentClient ] = availableClientIDs.splice(currentIndex, 1);

			// Send the request to the client and store the request promise.
			const requestPromise = this.clients[currentClient].connection.request(method, ...parameters);
			this.requestPromises[requestId].push(requestPromise);

			// Increase the sent counter.
			sentCounter += 1;
		}

		// Define a function to poll for request responses.
		const pollResponse = (resolve: ResolveFunction<RequestResponse>, reject: RejectFunction): void =>
		{
			// Define a function to resolve request responses based on integrity.
			const resolveRequest = async (): Promise<void> =>
			{
				// Set up an empty set of response data.
				const responseData: { [index: string]: number } = {};

				// Set up a counter to keep track of how many responses we have checked.
				let checkedResponses = 0;

				// For each server we issued a request to..
				for(const currentPromise in this.requestPromises[requestId])
				{
					// Race the request promise against a pre-resolved request to determine promise status.
					const promises = [ this.requestPromises[requestId][currentPromise], Promise.resolve(undefined) ];
					const response = await Promise.race(promises);

					// If the promise is settled..
					if(response)
					{
						// Calculate a unique identifier for this notification data.
						const responseDataIdentifier = JSON.stringify(response);

						// Increase the counter for checked responses.
						checkedResponses += 1;

						// Either set the response data counter or increase it.
						if(responseData[responseDataIdentifier] === undefined)
						{
							responseData[responseDataIdentifier] = 1;
						}
						else
						{
							responseData[responseDataIdentifier] += 1;
						}

						// Check if this response has enough integrity according to our confidence strategy.
						if(responseData[responseDataIdentifier] === this.strategy.confidence)
						{
							// Write log entry.
							debug.cluster(`Validated response for '${method}' with sufficient integrity (${this.strategy.confidence}).`);

							// Resolve the request with this response.
							resolve(response);

							// Return after resolving since we do not want to continue the execution.
							return;
						}
					}
				}

				// If all clients have responded but we failed to reach desired integrity..
				if(checkedResponses === this.requestPromises[requestId].length)
				{
					// Reject this request with an error message.
					reject(new Error(`Unable to complete request for '${method}', response failed to reach sufficient integrity (${this.strategy.confidence}).`));

					// Return after rejecting since we do not want to continue the execution.
					return;
				}

				// If we are not ready, but have not timed out and should wait more..
				setTimeout(resolveRequest, 1000);
			};

			// Attempt the initial resolution of the request.
			resolveRequest();
		};

		// return some kind of promise that resolves when integrity number of clients results match.
		return new Promise(pollResponse);
	}

	/**
	 * Subscribes to the method at the cluster and attaches the callback function to the event feed.
	 *
	 * @param {function}  callback     a function that should get notification messages.
	 * @param {string}    method       one of the subscribable methods the server supports.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if not enough clients are connected
	 * @throws {Error} if no response is received with sufficient integrity for the initial request
	 * @returns a promise resolving to true when the subscription is set up.
	 */
	async subscribe(callback: SubscribeCallback, method: string, ...parameters: RPCParameter[]): Promise<true>
	{
		// Define a function resolve the subscription setup process.
		const subscriptionResolver = async (resolve: ResolveFunction<true>): Promise<void> =>
		{
			// Define a callback function to validate server notifications and pass
			// them to the subscribe callback.
			const subscriptionResponder = async (data: Error | RequestResponse): Promise<void> =>
			{
				// Lock this response method temporarily.
				const unlock = await this.responseLock.acquire();

				try
				{
					// Calculate a unique identifier for this notification data.
					const responseDataIdentifier = JSON.stringify(data);

					// Either set the notification counter or increase it.
					if(this.notifications[responseDataIdentifier] === undefined)
					{
						this.notifications[responseDataIdentifier] = 1;
					}
					else
					{
						this.notifications[responseDataIdentifier] += 1;
					}

					// Check if this notification has enough integrity according to our confidence strategy.
					if(this.notifications[responseDataIdentifier] === this.strategy.confidence)
					{
						// Write log entry.
						debug.cluster(`Validated notification for '${method}' with sufficient integrity (${this.strategy.confidence}).`);

						// Send the notification data to the callback function.
						callback(data);
					}
				}
				finally
				{
					// Unlock the response method so it can handle the next set of data.
					unlock();
				}
			};

			// Set up event listener for this subscription.
			for(const currentClient in this.clients)
			{
				// Copy the current client for brevity.
				const client = this.clients[currentClient].connection;

				// If this method is not yet being listened on..
				if(!client.listeners(method).includes(subscriptionResponder))
				{
					// Set up event listener for this subscription.
					client.addListener(method, subscriptionResponder);
				}

				// If this method has never been subscribed to before..
				if(!client.subscriptionMethods[method])
				{
					// Initialize an empty subscription payload list for this method.
					client.subscriptionMethods[method] = [];
				}

				// Store the subscription parameters to track what data we have subscribed to.
				client.subscriptionMethods[method].push(JSON.stringify(parameters));

				// Get the currently subscribed payloads for this callback, or an empty array if none exist.
				const subscriptionCallbackPayloads = client.subscriptionCallbacks.get(callback) || [];

				// Update the subscription parameters to track what data this callback is listening on.
				subscriptionCallbackPayloads.push({ method, payload: JSON.stringify(parameters) });

				// Store the subscription parameters.
				client.subscriptionCallbacks.set(callback, subscriptionCallbackPayloads);
			}

			// Send initial subscription request.
			const requestData = await this.request(method, ...parameters);

			// Manually send the initial request data to the callback.
			callback(requestData);

			// Resolve the subscription promise.
			resolve(true);
		};

		// Return a promise that resolves when the subscription is set up.
		return new Promise(subscriptionResolver);
	}

	/**
	 * Unsubscribes to the method at the cluster and removes any callback functions
	 * when there are no more subscriptions for the method.
	 *
	 * @param {function}  callback     a function that has previously been subscribed for this method.
	 * @param {string}    method       one of the subscribable methods the server supports.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if, for any of the clients, no subscriptions exist for the combination of the
	 * passed `callback`, `method` and `parameters.
	 * @returns a promise resolving to true when the subscription has been cancelled.
	 */
	async unsubscribe(
		callback: SubscribeCallback,
		method: string,
		...parameters: RPCParameter[]
	): Promise<true>
	{
		// Define a function resolve the subscription setup process.
		const subscriptionResolver = async (resolve: ResolveFunction<true>): Promise<void> =>
		{
			// For each client..
			for(const currentClient in this.clients)
			{
				// Store client in variable for brevity
				const client = this.clients[currentClient].connection;

				// Log a warning if one of the clients is disconnected, but don't throw an error
				if(client.connection.status !== ConnectionStatus.CONNECTED)
				{
					debug.warning(`Client with server ${client.connection.host} could not be reached to unsubscribe`);
					continue;
				}

				// unsubscribe this client.
				client.unsubscribe(callback, method, ...parameters);
			}

			// Resolve the subscription promise.
			resolve(true);
		};

		// Return a promise that resolves when the subscription is set up.
		return new Promise(subscriptionResolver);
	}

	/**
	 * Provides a method to check or wait for the cluster to become ready.
	 *
	 * @returns a promise that resolves when the required servers are available.
	 */
	async ready(): Promise<boolean>
	{
		// Store the current timestamp.
		const readyTimestamp = Date.now();

		// Define a function to poll for availability of the cluster.
		const availabilityPoller = (resolve: ResolveFunction<boolean>): void =>
		{
			// Define a function to check if the cluster is ready to be used.
			const connectionAvailabilityVerifier = (): void =>
			{
				// Check if the cluster is active..
				if(this.status === ClusterStatus.READY)
				{
					// Resolve with true to indicate that the cluster is ready to use.
					resolve(true);

					// Return after resolving since we do not want to continue the execution.
					return;
				}

				// Calculate how long we have waited, in milliseconds.
				const timeWaited = (Date.now() - readyTimestamp);

				// Check if we have waited longer than our timeout setting.
				if(timeWaited > this.timeout)
				{
					// Resolve with false to indicate that we did not get ready in time.
					resolve(false);

					// Return after resolving since we do not want to continue the execution.
					return;
				}

				// If we are not ready, but have not timed out and should wait more..
				setTimeout(connectionAvailabilityVerifier, 50);
			};

			// Run the initial verification.
			connectionAvailabilityVerifier();
		};

		// Return a promise that resolves when the available clients is sufficient.
		return new Promise(availabilityPoller);
	}

	/**
	 * Connects all servers from the cluster and attaches event listeners and handlers
	 * for all underlying clients and connections.
	 *
	 * @throws {Error} if the cluster's version is not a valid version string.
	 */
	async startup(): Promise<void[]>
	{
		// Write a log message.
		debug.cluster('Starting up cluster.');

		// Keep track of all connections
		const connections = [];

		// Loop over all clients and reconnect them if they're disconnected
		for(const clientKey in this.clients)
		{
			// Retrieve connection information for the client
			const { host, port, scheme } = this.clients[clientKey].connection.connection;

			// Only connect currently unavailable/disconnected clients
			if(this.clients[clientKey].state === ClientState.AVAILABLE)
			{
				// Warn when a server is already connected when calling startup()
				debug.warning(`Called startup(), but server ${host}:${port} is already connected`);
			}
			else
			{
				// Call the addServer() function with the existing connection data
				// This effectively reconnects the server and re-instates all event listeners
				connections.push(this.addServer(host, port, scheme));
			}
		}

		// Await all connections
		return Promise.all(connections);
	}

	/**
	 * Disconnects all servers from the cluster. Removes all event listeners and
	 * handlers from all underlying clients and connections. This includes all
	 * active subscriptions, unless retainSubscriptions is set to true.
	 *
	 * @param {boolean} retainSubscriptions   retain subscription data so they will be restored on reconnection.
	 *
	 * @returns a list with the disconnection result for every client
	 */
	async shutdown(retainSubscriptions: boolean = false): Promise<boolean[]>
	{
		// Write a log message.
		debug.cluster('Shutting down cluster.');

		// Set up a list of disconnections to wait for.
		const disconnections: Promise<boolean>[] = [];

		const disconnectResolver = (resolve: ResolveFunction<boolean[]>): void =>
		{
			// Resolve once the cluster is marked as disabled
			this.once('disabled', () => resolve(Promise.all(disconnections)));

			// For each client in this cluster..
			for(const clientIndex in this.clients)
			{
				// Force disconnection regardless of current status.
				disconnections.push(this.clients[clientIndex].connection.disconnect(true, retainSubscriptions));
			}
		};

		// Return a list of booleans indicating disconnections from all clients
		return new Promise<boolean[]>(disconnectResolver);
	}
}

// Export the cluster.
export default ElectrumCluster;
