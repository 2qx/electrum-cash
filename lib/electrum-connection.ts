import debug from './util';
import ElectrumProtocol from './electrum-protocol';
import { ResolveFunction, RejectFunction, VersionRejected, VersionNegotiated, isVersionRejected, TransportScheme } from './interfaces';
import { DefaultParameters } from './constants';
import { EventEmitter } from 'events';
import ElectrumSocket from './electrum-socket';
import { ConnectionStatus } from './enums';

/**
 * Wrapper around TLS/WSS sockets that gracefully separates a network stream into Electrum protocol messages.
 *
 * @ignore
 */
class ElectrumConnection extends EventEmitter
{
	// Declare an empty socket.
	socket: ElectrumSocket;

	// Declare empty timestamps
	lastReceivedTimestamp: number;

	// Declare timers for keep-alive pings and reconnection
	timers: {
		// eslint-disable-next-line no-undef
		keepAlive?: NodeJS.Timer;
		// eslint-disable-next-line no-undef
		reconnect?: NodeJS.Timer;
	} = {};

	// Initialize an empty array of connection verification timers.
	// eslint-disable-next-line no-undef
	verifications: Array<NodeJS.Timer> = [];

	// Initialize the connected flag to false to indicate that there is no connection
	status: ConnectionStatus = ConnectionStatus.DISCONNECTED;

	// Initialize messageBuffer to an empty string
	messageBuffer = '';

	/**
	 * Sets up network configuration for an Electrum client connection.
	 *
	 * @param {string} application       your application name, used to identify to the electrum host.
	 * @param {string} version           protocol version to use with the host.
	 * @param {string} host              fully qualified domain name or IP number of the host.
	 * @param {number} port              the network port of the host.
	 * @param {TransportScheme} scheme   the transport scheme to use for connection
	 * @param {number} timeout           how long network delays we will wait for before taking action, in milliseconds.
	 * @param {number} pingInterval      the time between sending pings to the electrum host, in milliseconds.
	 *
	 * @throws {Error} if `version` is not a valid version string.
	 */
	constructor(
		public application: string,
		public version: string,
		public host: string,
		public port: number = DefaultParameters.PORT,
		public scheme: TransportScheme = DefaultParameters.TRANSPORT_SCHEME,
		public timeout: number = DefaultParameters.TIMEOUT,
		public pingInterval: number = DefaultParameters.PING_INTERVAL,
	)
	{
		// Initialize the event emitter.
		super();

		// Check if the provided version is a valid version number.
		if(!ElectrumProtocol.versionRegexp.test(version))
		{
			// Throw an error since the version number was not valid.
			throw(new Error(`Provided version string (${version}) is not a valid protocol version number.`));
		}

		// Create an initial network socket.
		this.createSocket();
	}

	/**
	 * Returns a string for the host identifier for usage in debug messages.
	 */
	get hostIdentifier(): string
	{
		return `${this.host}:${this.port}`;
	}

	/**
	 * Create and configures a fresh socket and attaches all relevant listeners.
	 */
	createSocket(): void
	{
		// Initialize a new ElectrumSocket
		this.socket = new ElectrumSocket();

		// Set up handlers for connection and disconnection.
		this.socket.on('connect', this.onSocketConnect.bind(this));
		this.socket.on('disconnect', this.onSocketDisconnect.bind(this));

		// Set up handler for incoming data.
		this.socket.on('data', this.parseMessageChunk.bind(this));
	}

	/**
	 * Shuts down and destroys the current socket.
	 */
	destroySocket(): void
	{
		// Close the socket connection and destroy the socket.
		this.socket.disconnect();
	}

	/**
	 * Assembles incoming data into statements and hands them off to the message parser.
	 *
	 * @param {string} data   data to append to the current message buffer, as a string.
	 *
	 * @throws {SyntaxError} if the passed statement parts are not valid JSON.
	 */
	parseMessageChunk(data: string): void
	{
		// Update the timestamp for when we last received data.
		this.lastReceivedTimestamp = Date.now();

		// Clear and remove all verification timers.
		this.verifications.forEach((timer) => clearTimeout(timer));
		this.verifications.length = 0;

		// Add the message to the current message buffer.
		this.messageBuffer += data;

		// Check if the new message buffer contains the statement delimiter.
		while(this.messageBuffer.includes(ElectrumProtocol.statementDelimiter))
		{
			// Split message buffer into statements.
			const statementParts = this.messageBuffer.split(ElectrumProtocol.statementDelimiter);

			// For as long as we still have statements to parse..
			while(statementParts.length > 1)
			{
				// Move the first statement to its own variable.
				const currentStatementList = String(statementParts.shift());

				// Parse the statement into an object or list of objects.
				let statementList = JSON.parse(currentStatementList);

				// Wrap the statement in an array if it is not already a batched statement list.
				if(!Array.isArray(statementList))
				{
					statementList = [ statementList ];
				}

				// For as long as there is statements in the result set..
				while(statementList.length > 0)
				{
					// Move the first statement from the batch to its own variable.
					const currentStatement = statementList.shift();

					// If the current statement is a version negotiation response..
					if(currentStatement.id === 'versionNegotiation')
					{
						if(currentStatement.error)
						{
							// Then emit a failed version negotiation response signal.
							this.emit('version', { error: currentStatement.error });
						}
						else
						{
							// Emit a successful version negotiation response signal.
							this.emit('version', { software: currentStatement.result[0], protocol: currentStatement.result[1] });
						}

						// Consider this statement handled.
						continue;
					}

					// If the current statement is a keep-alive response..
					if(currentStatement.id === 'keepAlive')
					{
						// Do nothing and consider this statement handled.
						continue;
					}

					// Emit the statements for handling higher up in the stack.
					this.emit('statement', currentStatement);
				}
			}

			// Store the remaining statement as the current message buffer.
			this.messageBuffer = statementParts.shift() || '';
		}
	}

	/**
	 * Sends a keep-alive message to the host.
	 *
	 * @returns true if the ping message was fully flushed to the socket, false if
	 * part of the message is queued in the user memory
	 */
	ping(): boolean
	{
		// Write a log message.
		debug.ping(`Sending keep-alive ping to '${this.hostIdentifier}'`);

		// Craft a keep-alive message.
		const message = ElectrumProtocol.buildRequestObject('server.ping', [], 'keepAlive');

		// Send the keep-alive message.
		const status = this.send(message);

		// Return the ping status.
		return status;
	}

	/**
	 * Initiates the network connection negotiates a protocol version. Also emits the 'connect' signal if successful.
	 *
	 * @throws {Error} if the socket connection fails.
	 * @returns a promise resolving when the connection is established
	 */
	async connect(): Promise<void>
	{
		// If we are already connected return true.
		if(this.status === ConnectionStatus.CONNECTED)
		{
			return;
		}

		// Indicate that the connection is connecting
		this.status = ConnectionStatus.CONNECTING;

		// Define a function to wrap connection as a promise.
		const connectionResolver = (resolve: ResolveFunction<void>, reject: RejectFunction): void =>
		{
			const rejector = (error: Error): void =>
			{
				// Set the status back to disconnected
				this.status = ConnectionStatus.DISCONNECTED;

				// Reject with the error as reason
				reject(error);
			};

			// Replace previous error handlers to reject the promise on failure.
			this.socket.removeAllListeners('error');
			this.socket.once('error', rejector);

			// Define a function to wrap version negotiation as a callback.
			const versionNegotiator = (): void =>
			{
				// Write a log message to show that we have started version negotiation.
				debug.network(`Requesting protocol version ${this.version} with '${this.hostIdentifier}'.`);

				// remove the one-time error handler since no error was detected.
				this.socket.removeListener('error', rejector);

				// Build a version negotiation message.
				const versionMessage = ElectrumProtocol.buildRequestObject('server.version', [ this.application, this.version ], 'versionNegotiation');

				// Define a function to wrap version validation as a function.
				const versionValidator = (version: VersionRejected | VersionNegotiated): void =>
				{
					// Check if version negotiation failed.
					if(isVersionRejected(version))
					{
						// Disconnect from the host.
						this.disconnect(true);

						// Declare an error message.
						const errorMessage = 'unsupported protocol version.';

						// Log the error.
						debug.errors(`Failed to connect with ${this.hostIdentifier} due to ${errorMessage}`);

						// Reject the connection with false since version negotiation failed.
						reject(errorMessage);
					}
					// Check if the host supports our requested protocol version.
					else if(version.protocol !== this.version)
					{
						// Disconnect from the host.
						this.disconnect(true);

						// Declare an error message.
						const errorMessage = `incompatible protocol version negotiated (${version.protocol} !== ${this.version}).`;

						// Log the error.
						debug.errors(`Failed to connect with ${this.hostIdentifier} due to ${errorMessage}`);

						// Reject the connection with false since version negotiation failed.
						reject(errorMessage);
					}
					else
					{
						// Write a log message.
						debug.network(`Negotiated protocol version ${version.protocol} with '${this.hostIdentifier}', powered by ${version.software}.`);

						// Set connection status to connected
						this.status = ConnectionStatus.CONNECTED;

						// Emit a connect event now that the connection is usable.
						this.emit('connect');

						// Resolve the connection promise since we successfully connected and negotiated protocol version.
						resolve();
					}
				};

				// Listen for version negotiation once.
				this.once('version', versionValidator);

				// Send the version negotiation message.
				this.send(versionMessage);
			};

			// Prepare the version negotiation.
			this.socket.once('connect', versionNegotiator);

			// Set up handler for network errors.
			this.socket.on('error', this.onSocketError.bind(this));

			// Connect to the server.
			this.socket.connect(this.host, this.port, this.scheme, this.timeout);
		};

		// Wait until connection is established and version negotiation succeeds.
		await new Promise<void>(connectionResolver);
	}

	/**
	 * Restores the network connection.
	 */
	async reconnect(): Promise<void>
	{
		// If a reconnect timer is set, remove it
		await this.clearReconnectTimer();

		// Write a log message.
		debug.network(`Trying to reconnect to '${this.hostIdentifier}'..`);

		// Set the status to reconnecting for more accurate log messages.
		this.status = ConnectionStatus.RECONNECTING;

		// Destroy and recreate the socket to get a clean slate.
		this.destroySocket();
		this.createSocket();

		try
		{
			// Try to connect again.
			await this.connect();
		}
		catch(error)
		{
			// Do nothing as the error should be handled via the disconnect and error signals.
		}
	}

	/**
	 * Removes the current reconnect timer.
	 */
	clearReconnectTimer(): void
	{
		// If a reconnect timer is set, remove it
		if(this.timers.reconnect)
		{
			clearTimeout(this.timers.reconnect);
		}

		// Reset the timer reference.
		this.timers.reconnect = undefined;
	}

	/**
	 * Removes the current keep-alive timer.
	 */
	clearKeepAliveTimer(): void
	{
		// If a keep-alive timer is set, remove it
		if(this.timers.keepAlive)
		{
			clearTimeout(this.timers.keepAlive);
		}

		// Reset the timer reference.
		this.timers.keepAlive = undefined;
	}

	/**
	 * Initializes the keep alive timer loop.
	 */
	setupKeepAliveTimer(): void
	{
		// If the keep-alive timer loop is not currently set up..
		if(!this.timers.keepAlive)
		{
			// Set a new keep-alive timer.
			this.timers.keepAlive = setTimeout(this.ping.bind(this), this.pingInterval);
		}
	}

	/**
	 * Tears down the current connection and removes all event listeners on disconnect.
	 *
	 * @param {boolean} force   disconnect even if the connection has not been fully established yet.
	 *
	 * @returns true if successfully disconnected, or false if there was no connection.
	 */
	async disconnect(force: boolean = false): Promise<boolean>
	{
		// Return early when there is nothing to disconnect from
		if(this.status === ConnectionStatus.DISCONNECTED && !force)
		{
			// Return false to indicate that there was nothing to disconnect from.
			return false;
		}

		// Set connection status to null to indicate tear-down is currently happening.
		this.status = ConnectionStatus.DISCONNECTING;

		// If a keep-alive timer is set, remove it.
		await this.clearKeepAliveTimer();

		// If a reconnect timer is set, remove it
		await this.clearReconnectTimer();

		const disconnectResolver = (resolve: ResolveFunction<boolean>): void =>
		{
			// Resolve to true after the connection emits a disconnect
			this.once('disconnect', () => resolve(true));

			// Close the connection and destroy the socket.
			this.destroySocket();
		};

		// Return true to indicate that we disconnected.
		return new Promise<boolean>(disconnectResolver);
	}

	/**
	 * Sends an arbitrary message to the server.
	 *
	 * @param {string} message   json encoded request object to send to the server, as a string.
	 *
	 * @returns true if the message was fully flushed to the socket, false if part of the message
	 * is queued in the user memory
	 */
	send(message: string): boolean
	{
		// Remove the current keep-alive timer if it exists.
		this.clearKeepAliveTimer();

		// Get the current timestamp in milliseconds.
		const currentTime = Date.now();

		// Follow up and verify that the message got sent..
		const verificationTimer = setTimeout(this.verifySend.bind(this, currentTime), this.timeout);

		// Store the verification timer locally so that it can be cleared when data has been received.
		this.verifications.push(verificationTimer);

		// Set a new keep-alive timer.
		this.setupKeepAliveTimer();

		// Write the message to the network socket.
		return this.socket.write(message + ElectrumProtocol.statementDelimiter);
	}

	// --- Event managers. --- //

	/**
	 * Marks the connection as timed out and schedules reconnection if we have not
	 * received data within the expected time frame.
	 */
	verifySend(sentTimestamp: number): void
	{
		// If we haven't received any data since we last sent data out..
		if(Number(this.lastReceivedTimestamp) < sentTimestamp)
		{
			// If this connection is already disconnected, we do not change anything
			if((this.status === ConnectionStatus.DISCONNECTED) || (this.status === ConnectionStatus.DISCONNECTING))
			{
				debug.errors(`Tried to verify already disconnected connection to '${this.hostIdentifier}'`);

				return;
			}

			// Remove the current keep-alive timer if it exists.
			this.clearKeepAliveTimer();

			// Write a notification to the logs.
			debug.network(`Connection to '${this.hostIdentifier}' timed out.`);

			// Close the connection to avoid re-use.
			// NOTE: This initiates reconnection routines if the connection has not
			//       been marked as intentionally disconnected.
			this.socket.disconnect();
		}
	}

	/**
	 * Updates the connection status when a connection is confirmed.
	 */
	onSocketConnect(): void
	{
		// If a reconnect timer is set, remove it.
		this.clearReconnectTimer();

		// Set up the initial timestamp for when we last received data from the server.
		this.lastReceivedTimestamp = Date.now();

		// Set up the initial keep-alive timer.
		this.setupKeepAliveTimer();

		// Clear all temporary error listeners.
		this.socket.removeAllListeners('error');

		// Set up handler for network errors.
		this.socket.on('error', this.onSocketError.bind(this));
	}

	/**
	 * Updates the connection status when a connection is ended.
	 */
	onSocketDisconnect(): void
	{
		// Send a disconnect signal higher up the stack.
		this.emit('disconnect');

		// Remove the current keep-alive timer if it exists.
		this.clearKeepAliveTimer();

		// If this is a connection we're trying to tear down..
		if(this.status === ConnectionStatus.DISCONNECTING)
		{
			// If a reconnect timer is set, remove it.
			this.clearReconnectTimer();

			// Remove all event listeners
			this.removeAllListeners();

			// Mark the connection as disconnected.
			this.status = ConnectionStatus.DISCONNECTED;

			// Write a log message.
			debug.network(`Disconnected from '${this.hostIdentifier}'.`);
		}
		else
		{
			// If this is for an established connection..
			if(this.status === ConnectionStatus.CONNECTED)
			{
				// Write a notification to the logs.
				debug.errors(`Connection with '${this.hostIdentifier}' was closed, trying to reconnect in ${DefaultParameters.RECONNECT / 1000} seconds.`);
			}
			// If this is a connection that is currently connecting, reconnecting or already disconnected..
			else
			{
				// Do nothing

				// NOTE: This error message is useful during manual debugging of reconnections.
				// debug.errors(`Lost connection with reconnecting or already disconnected server '${this.hostIdentifier}'.`);
			}

			// Mark the connection as disconnected for now..
			this.status = ConnectionStatus.DISCONNECTED;

			// If we don't have a pending reconnection timer..
			if(!this.timers.reconnect)
			{
				// Attempt to reconnect after one keep-alive duration.
				this.timers.reconnect = setTimeout(this.reconnect.bind(this), DefaultParameters.RECONNECT);
			}
		}
	}

	/**
	 * Notify administrator of any unexpected errors.
	 */
	onSocketError(error: Error | any): void
	{
		// If the DNS lookup failed.
		if(error.code === 'EAI_AGAIN')
		{
			debug.errors(`Failed to look up DNS records for '${this.host}'.`);
		}
		// If the connection timed out..
		else if(error.code === 'ETIMEDOUT')
		{
			// Log the provided timeout message.
			debug.errors(error.message);
		}
		else
		{
			// Log unknown error
			debug.errors(`Unknown network error ('${this.hostIdentifier}'): `, error);
		}
	}
}

// Export the connection.
export default ElectrumConnection;
