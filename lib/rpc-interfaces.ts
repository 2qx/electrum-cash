// Acceptable parameter types for RPC messages
export type RPCParameter = string | number | boolean | null;

// The base type for all RPC messages
export interface RPCBase
{
	jsonrpc: string;
}

// An RPC message that sends a notification requiring no response
export interface RPCNotification extends RPCBase
{
	method: string;
	params?: RPCParameter[];
}

// An RPC message that sends a request requiring a response
export interface RPCRequest extends RPCBase
{
	id: number | null;
	method: string;
	params?: RPCParameter[];
}

// An RPC message that returns the response to a successful request
export interface RPCStatement extends RPCBase
{
	id: number | null;
	result: string;
}

export interface RPCError
{
	code: number;
	message: string;
	data?: any;
}

// An RPC message that returns the error to an unsuccessful request
export interface RPCErrorResponse extends RPCBase
{
	id: number | null;
	error: RPCError;
}

// A response to a request is either a statement (successful) or an error (unsuccessful)
export type RPCResponse = RPCErrorResponse | RPCStatement;

// RPC messages are notifications, requests, or responses
export type RPCMessage = RPCNotification | RPCRequest | RPCResponse;

// Requests and responses can also be sent in batches
export type RPCResponseBatch = RPCResponse[];
export type RPCRequestBatch = RPCRequest[];

export const isRPCErrorResponse = function(message: RPCBase): message is RPCErrorResponse
{
	return 'id' in message && 'error' in message;
};

export const isRPCStatement = function(message: RPCBase): message is RPCStatement
{
	return 'id' in message && 'result' in message;
};

export const isRPCNotification = function(message: RPCBase): message is RPCNotification
{
	return !('id' in message) && 'method' in message;
};

export const isRPCRequest = function(message: RPCBase): message is RPCRequest
{
	return 'id' in message && 'method' in message;
};
