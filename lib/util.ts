import { debug as debugLogger } from 'debug';

// Create the debug logs.
const debug =
{
	client:  debugLogger('electrum-cash:client '),
	cluster: debugLogger('electrum-cash:cluster'),
	errors:  debugLogger('electrum-cash:error  '),
	warning: debugLogger('electrum-cash:warning'),
	network: debugLogger('electrum-cash:network'),
	ping:    debugLogger('electrum-cash:pulses '),
};

// Set log colors.
debug.client.color = '2';
debug.cluster.color = '3';
debug.errors.color = '9';
debug.warning.color = '13';
debug.network.color = '4';
debug.ping.color = '8';

// Export the logs.
export default debug;
