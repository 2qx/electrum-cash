// Import package.json to reduce name repetition.
// eslint-disable-next-line import/extensions
import pkg from './package.json';
import dts from 'rollup-plugin-dts';

export default
[
	{
		input: 'dist/module/lib/index.js',
		external: [ 'async-mutex', 'debug', 'isomorphic-ws', 'ws' ],
		output: [
			{ file: pkg.module, format: 'es' },
			{ file: pkg.main, format: 'cjs' },
		],
	},
	{
		input: 'dist/module/lib/index.d.ts',
		output: [{ file: pkg.typings, format: 'es' }],
		plugins: [ dts() ],
	},
];
